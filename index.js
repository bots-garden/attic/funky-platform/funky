const express = require('express')
const cp = require('child_process')
const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const multer  = require('multer')

// --- WIP 🚧 --- PROMETHEUS ---
const prometheus = require('prom-client')

const app = express()
const port = process.env.PORT || 9090
const functionsPath = process.env.FUNCTIONS_PATH || `./functions/production`
const javaPolicy = process.env.JAVA_POLICY || `../funky.policy`
const platform = process.env.PLATFORM || `PRODUCTION`
const killDelay = process.env.DELAY || 5

app.use(express.static('public/build/es6-bundled'))
app.use(express.json())

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, functionsPath)
  },
  filename: (req, file, cb) => {
    fs.mkdir(`${functionsPath}/${req.body.function}-${req.body.branch}`, 
      { recursive: true }, 
      (err) => { 
        if (err) {
          // TODO: directory already exists
        }
        cb(null, `/${req.body.function}-${req.body.branch}/${file.originalname}`)
      }
    )
  }
})

const upload = multer({ storage: storage })

// --- WIP 🚧 --- PROMETHEUS ---
// * This is a test *
// * log calls, errors, durations, ...s
let gauge01 = new prometheus.Gauge({ name: 'yo', help: 'my amazing yo metric' });
gauge01.set(0)

/*
http://localhost:9090/function/toto/master/%22bob%20morane%22
http://localhost:9090/function/hello/master/%22bob%20morane%22
*/

let executeFunction = (req, res) => {
  try {
    let name = req.params["name"]
    let branch = req.params["branch"] // 👋 I called it branch but you can use it for a lot of things (eg: user name)
    
    // TODO: POST version
    let params = req.params["params"]==null ? "" : req.params["params"]
    let path = `${functionsPath}/${name}-${branch}`
    let content = yaml.safeLoad(fs.readFileSync(`${path}/config.yml`, 'utf8'))
    let jarFile = content["jar-file"]
    let jvmOptions = `-Djava.security.manager -Djava.security.policy==${javaPolicy}`

    //let command = `java -jar ${jvmOptions} ${path}/${jarFile} ${params}`
    let processName = `${name}-${branch}`

    let command = `timeout -k ${killDelay} ${killDelay} java -jar ${jvmOptions} ${path}/${jarFile} ${params}`

    let childProcess = cp.exec(`${command}`, (error, stdout, stderr) => {
      // --- WIP 🚧 --- PROMETHEUS ---
      // TODO: count the errors, etc......
      gauge01.inc(1)

      if(error) {
        res.set('Content-Type', 'text/plain')
        res.status(500)
        //res.send(stderr) // TODO: better error message
        res.send(`💥 ${processName} 😢 you propably did something very bad 😡 ${childProcess.pid} ${error.message}`)
      } else {
      
        switch(content["content-type"]) {
          case "json":
            res.json({result: stdout})
            //res.json(stdout)
            break
          case "html":
            res.set('Content-Type', 'text/html')
            res.send(stdout)
            break
          case "text":
            res.set('Content-Type', 'text/plain')
            res.send(stdout)
            break
          default:
            res.json({result: stdout})
        }
      }
    })

  } catch (error) {
    res.set('Content-Type', 'text/plain')
    res.status(500)
    res.send(error.message) // TODO: better error message
  }
}

// --- WIP 🚧 --- PROMETHEUS ---
app.get('/metrics', (req, res) => {
  res.set(prometheus.register.contentType)
  res.send(prometheus.register.metrics())
})

app.get('/function/:name/:branch/:params', (req, res) => {
  executeFunction(req, res)
})

app.get('/function/:name/:branch', (req, res) => {
  executeFunction(req, res)
})

app.post('/function/:name/:branch', (req, res) => {
  // TODO: ⚠️
  executeFunction(req, res)
})

// --- upload a function --
// - a function = a jar file + config.yml
app.post('/publish', upload.array('file'), (req,res) => {
  if(req.get("ADMIN_TOKEN")==process.env.ADMIN_TOKEN) {
    res.set('Content-Type', 'text/plain')
    res.send(req.file) // TODO: how to display a message (check how to have a verbose curl)
  } else {
    res.status(500)
    res.send("BAD TOKEN") // TODO: better error message
  }
})

// --- remove a function --
app.delete('/function/:name/:branch', (req, res) => {

  res.set('Content-Type', 'text/plain')

  if(req.get("ADMIN_TOKEN")==process.env.ADMIN_TOKEN) {
    try {
      let name = req.params["name"]
      let branch = req.params["branch"] 
      let params = req.params["params"]
      let path = `${functionsPath}/${name}-${branch}`

      cp.exec(`rm -rf ${path}`, (error, stdout, stderr) => {
        if(error) {
          res.status(500)
          res.send(stderr) // TODO: better error message
        } else {
          res.send("OK")
        }
      })    
    } catch (error) {
      res.status(500)
      res.send(error) // TODO: better error message
    }
  } else {
    res.status(500)
    res.send("BAD TOKEN") // TODO: better error message
  }

})

// TODO: get the list of the functions
// TODO: create an admin webapp
// TODO: find a hoster

 
// define a route to download a file 
app.get('/tools/:file(*)',(req, res) => {
  var file = req.params.file
  var fileLocation = path.join('./tools',file)
  console.log(fileLocation)
  res.download(fileLocation, file)
})

// TODO: better (nicer) message
app.get('/about', (req, res) => {
  res.set('Content-Type', 'text/plain')
  res.send(`FUNKY-JS-0.0.0 🚀🦊 [${platform}]`)
})

app.listen(port, () => console.log(`🌍 funky-js [${platform}] is listening on port ${port}!`))