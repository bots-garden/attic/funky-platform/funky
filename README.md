# Funky

> A little faas platform

The aim of this project is only "educative"

**Funky** platform is only a webapplication that runs **jar** files and sends the output as a response to your navigator

You need a linux platform to operate **Funky** platform (you can find a Vagrantfile in this repository).

If you want to run it on OX, fist install this:

```shell
brew install coreutils
sudo ln -s /usr/local/bin/gtimeout /usr/local/bin/timeout
```

## Start Funky

### First, install dependencies

```shell
npm install
```

### Then run it

```shell
ADMIN_TOKEN="SPACESQUID" \
npm start
# default http port is 9090
# default directory functions is `./functions/production`
```

If you need several **Funky** platforms:

```shell
ADMIN_TOKEN="SPACECOW" \
PORT=9091 \
PLATFORM="DEVELOPMENT" \
FUNCTIONS_PATH="./functions/development" \
npm start
```

## If you want to use it "from outside"

> use case: deploy locally with GitLab shared runners from GitLab.com

I used to use **[ngrok](https://ngrok.com/)** like that:

```shell
./ngrok authtoken <my_token>
./ngrok http -region=eu -subdomain=funky 127.0.0.1:9090 
echo "✅ http://funky.eu.ngrok.io/"

./ngrok authtoken <my_token>
./ngrok http -region=eu -subdomain=funkydev 127.0.0.1:9091 > /dev/null &
echo "✅ http://funkydev.eu.ngrok.io/"
```

## Write a function

See some examples in `./functions/production`

## Deploy a function

See the "sister" project: https://gitlab.com/bots-garden/funky-platform/funky-cli