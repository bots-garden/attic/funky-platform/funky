import { LitElement, html } from 'lit-element'
import {render} from 'lit-html'
import {MyTitle} from './my-title.js'
import {styles} from './styles.js';

export class MainApplication extends LitElement {

  constructor() {
    super()
    render(html`
      ${styles}
      <style>html,body { font-family: 'robotoregular';}</style>
    `, document.head)  
  }

  render() {
    return html`
    ${styles}
      <section class="section">
        <div class="container">
          <my-title></my-title>
        </div>
      </section>
    `
  }
}

customElements.define('main-application', MainApplication)
